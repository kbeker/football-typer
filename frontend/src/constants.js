export const BackendEndpointUrls = {
    register: "/api/v1/register/",
    obtainToken: "/api/v1/token/obtain/",
    teams: "/api/v1/teams/",
    players: "/api/v1/players/",
    games: "/api/v1/games/",
    gamePredictions: "/api/v1/game-prediction/",
    gamePredictionsByGameId: "/api/v1/game-prediction-by-game-id/",
    userPredictionsByUserId: "/api/v1/user-predictions-by-user-id/",
    mostScorerPrediction: "/api/v1/most-scorer-prediction/",
    mostAssistsPrediction: "/api/v1/most-assists-prediction/",
    mostRedCardPlayerPrediction: "/api/v1/most-red-cards-player-prediction/",
    mostYellowCardPlayerPrediction: "/api/v1/most-yellow-cards-player-prediction/",
    worldCupWinnerPrediction: "/api/v1/world-cup-winner-prediction/",
    mostRedCardsTeamPrediction: "/api/v1/most-red-cards-team-prediction/",
    mostYellowCardsTeamPrediction: "/api/v1/most-yellow-cards-team-prediction/",
    firstGameDateTime: "/api/v1/first-game-date-time/",
    user: "/api/v1/user/",
    ranking: "/api/v1/ranking/",
    config: "/api/v1/config/",
}

export const Tokens = {
    access: "accessToken",
    refresh: "refreshToken",
}

export const FrontendUrls = {
    login: "/login",
    registration: "/register",
    games: "/",
    ranking: "/ranking",
}
