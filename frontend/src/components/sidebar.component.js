import "./sidebar.component.css"
import SidebarElementComponent from "./sidebarElement.component"
import { DoorClosed, Joystick, PersonFill, Trophy } from "react-bootstrap-icons"
import { redirectLogin } from "../helpers"
import { FrontendUrls } from "../constants"
import UserService from "../services/user.service"
import AuthService from "../services/authorization.service"
import { useEffect } from "react"

export default function SidebarComponent(props) {
    function deleteToken() {
        window.localStorage.setItem("DELETE_TOKENS", Date.now().toString())
        window.sessionStorage.removeItem("accessToken")
        window.sessionStorage.removeItem("refreshToken")
        redirectLogin()
    }

    function fetchUserData() {
        UserService.getUser(AuthService.getUserId()).then(function (response) {
            const userDataDiv = document.getElementById("user-data")
            userDataDiv.innerHTML = `${response.data.first_name} ${response.data.last_name}`
        })
    }

    useEffect(() => {
        fetchUserData()
    })

    return (
        <div className={"sidebar"}>
            <div className={"user-details"}>
                <PersonFill />
                <p id={"user-data"} />
            </div>
            <div className={"navigation-links"}>
                <nav>
                    {/*<SidebarElementComponent to="/" toName="Home" icon={<House />} />*/}
                    {/*<SidebarElementComponent to="/ranking" toName="Ranking" icon={<PersonFill />} />*/}
                    <SidebarElementComponent to={FrontendUrls.games} toName="Games" icon={<Joystick />} />
                    <SidebarElementComponent to={FrontendUrls.ranking} toName="Ranking" icon={<Trophy />} />
                    <SidebarElementComponent toName="Logout" icon={<DoorClosed />} onClickAction={deleteToken} />
                </nav>
            </div>
        </div>
    )
}
