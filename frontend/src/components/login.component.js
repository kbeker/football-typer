import "./login.component.css"
import CustomContainer from "../commons/customContainer"
import { useState } from "react"
import { useNavigate } from "react-router-dom"
import AuthService from "../services/authorization.service"
import { FrontendUrls } from "../constants"
import { setInfoMessage } from "../commons/helpers"

export default function LoginComponent(props) {
    const [email, setEmail] = useState()
    const [password, setPassword] = useState()
    const navigate = useNavigate()

    function onClickInfoAuthDiv() {
        let infoDiv = document.getElementById("info-auth-div")
        infoDiv.classList.add("disabled")
    }

    function handleLogin(event) {
        event.preventDefault()
        AuthService.obtainToken({
            username: email,
            password: password,
        })
            .then(function (success) {
                if (success) {
                    navigate("/")
                }
            })
            .catch((response) => {
                if (response.response.data.detail) {
                    setInfoMessage("error", `${response.response.data.detail}`)
                } else if (response.response.data) {
                    let errors = ""
                    for (const property in response.response.data) {
                        errors += `${response.response.data[property]} <br/>`
                    }
                    setInfoMessage("error", `${errors}`)
                } else if (response.response.data.name) {
                    setInfoMessage("error", `${response.response.data.name}`)
                } else if (response.response.data.non_field_errors) {
                    setInfoMessage("error", `${response.response.data.non_field_errors}`)
                } else {
                    setInfoMessage("error", `${response.message}`)
                }
            })
    }

    return (
        <div className={"login-container"}>
            <div id={"info-auth-div"} className={"info-auth-div disabled"} onClick={onClickInfoAuthDiv} />
            <CustomContainer
                containerName={<div className={"container-details"}>Login</div>}
                containerBody={
                    <div className={"container-body"}>
                        <div className={"sign-up-form"}>
                            <form className="form" onSubmit={handleLogin}>
                                <input
                                    className="input"
                                    placeholder="Email"
                                    type="text"
                                    onChange={(event) => setEmail(event.target.value.toLowerCase())}
                                />
                                <input
                                    className="input"
                                    placeholder="Password"
                                    type="password"
                                    onChange={(event) => setPassword(event.target.value)}
                                />
                                <input className="confirm-button" type="submit" value="Login" />
                            </form>
                        </div>
                    </div>
                }
                containerBottom={
                    <div>If you want to register use {<a href={FrontendUrls.registration}>this link</a>}</div>
                }
            />
        </div>
    )
}
