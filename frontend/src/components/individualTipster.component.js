import "./individualTipster.component.css"
import GamesService from "../services/games.service"
import { useEffect, useState } from "react"
import AuthService from "../services/authorization.service"
import { setInfoMessage } from "../helpers"

export default function IndividualTipsterComponent(props) {
    const [player, setPlayer] = useState()
    const [bestScorerPredictionId, setBestScorerPredictionId] = useState()
    const [bestAssistantPredictionId, setBestAssistantPredictionId] = useState()
    const [worldCupWinnerPredictionId, setWorldCupWinnerPredictionId] = useState()
    const [mostRedCardPlayerPredictionId, setMostRedCardPlayerPredictionId] = useState()
    const [mostYellowCardPlayerPredictionId, setMostYellowCardPlayerPredictionId] = useState()
    const [mostRedCardTeamPredictionId, setMostRedCardTeamPredictionId] = useState()
    const [mostYellowCardTeamPredictionId, setMostYellowCardTeamPredictionId] = useState()
    const [inputsDisabled, setInputsDisabled] = useState(false)

    function fetchConfig() {}

    function fetchPlayers(numberOfTeams) {
        GamesService.getPlayers().then(function (response) {
            updateSelect(response.data, "bestScorerSelect")
            updateSelect(response.data, "bestAssistantSelect")
            updateSelect(response.data, "mostRedCardPlayerSelect")
            updateSelect(response.data, "mostYellowCardPlayerSelect")
            fetchPredictions(response.data.length + 1, numberOfTeams + 1)
        })
    }

    function fetchTeams() {
        GamesService.getTeams().then(function (response) {
            updateSelect(response.data, "worldCupWinnerSelect")
            updateSelect(response.data, "mostRedCardTeamSelect")
            updateSelect(response.data, "mostYellowCardTeamSelect")
            fetchPlayers(response.data.length)
        })
    }

    function updateSelect(data, selectName) {
        let select = document.getElementById(selectName)
        let currentValues = []

        for (let i = 0; i < select.options.length; i++) {
            currentValues.push(select.options[i].value)
        }

        for (let i = 0; i < data.length; i++) {
            if (!currentValues.includes(data[i].name)) {
                let option = document.createElement("option")
                option.id = data[i].id
                option.value = data[i].name
                option.innerHTML = data[i].name
                select.appendChild(option)
            }
        }
    }

    function configureInputs() {
        GamesService.getFirstGameDateTime().then(function (response) {
            GamesService.getConfig().then(function (configResponse) {
                if (
                    Date.now() > new Date(response.data.date_time) &&
                    !configResponse.data.ALLOW_OUTDATED_INDIVIDUAL_PREDICTIONS
                ) {
                    setInputsDisabled(true)
                }
            })
        })
    }

    useEffect(() => {
        fetchTeams()
        configureInputs()
    }, [])

    function fetchPredictions(numberOfPlayers, numberOfTeams) {
        GamesService.getUserPredictions(AuthService.getUserId()).then(function (response) {
            if (response.data.most_scorer_prediction) {
                setBestScorerPredictionId(response.data.most_scorer_prediction["id"])
                let bestScorerSelect = document.getElementById("bestScorerSelect")
                for (let i = 0; i < numberOfPlayers; i++) {
                    if (
                        Number(bestScorerSelect.options[i].id) ===
                        Number(response.data.most_scorer_prediction["player"])
                    ) {
                        bestScorerSelect.selectedIndex = i
                        break
                    }
                }
            }
            if (response.data.most_assists_prediction) {
                setBestAssistantPredictionId(response.data.most_assists_prediction["id"])
                let bestAssistantSelect = document.getElementById("bestAssistantSelect")
                for (let i = 0; i < numberOfPlayers; i++) {
                    if (
                        Number(bestAssistantSelect.options[i].id) ===
                        Number(response.data.most_assists_prediction["player"])
                    ) {
                        bestAssistantSelect.selectedIndex = i
                        break
                    }
                }
            }
            if (response.data.world_cup_winner_prediction) {
                setWorldCupWinnerPredictionId(response.data.world_cup_winner_prediction["id"])
                let worldCupWinnerSelect = document.getElementById("worldCupWinnerSelect")
                for (let i = 0; i < numberOfTeams; i++) {
                    if (
                        Number(worldCupWinnerSelect.options[i].id) ===
                        Number(response.data.world_cup_winner_prediction["team"])
                    ) {
                        worldCupWinnerSelect.selectedIndex = i
                        break
                    }
                }
            }
            if (response.data.most_red_cards_player_prediction) {
                setMostRedCardPlayerPredictionId(response.data.most_red_cards_player_prediction["id"])
                let mostRedCardPlayerSelect = document.getElementById("mostRedCardPlayerSelect")
                for (let i = 0; i < numberOfPlayers; i++) {
                    if (
                        Number(mostRedCardPlayerSelect.options[i].id) ===
                        Number(response.data.most_red_cards_player_prediction["player"])
                    ) {
                        mostRedCardPlayerSelect.selectedIndex = i
                        break
                    }
                }
            }
            if (response.data.most_yellow_cards_player_prediction) {
                setMostYellowCardPlayerPredictionId(response.data.most_yellow_cards_player_prediction["id"])
                let mostYellowCardPlayerSelect = document.getElementById("mostYellowCardPlayerSelect")
                for (let i = 0; i < numberOfPlayers; i++) {
                    if (
                        Number(mostYellowCardPlayerSelect.options[i].id) ===
                        Number(response.data.most_yellow_cards_player_prediction["player"])
                    ) {
                        mostYellowCardPlayerSelect.selectedIndex = i
                        break
                    }
                }
            }
            if (response.data.most_red_cards_team_prediction) {
                setMostRedCardTeamPredictionId(response.data.most_red_cards_team_prediction["id"])
                let mostRedCardTeamSelect = document.getElementById("mostRedCardTeamSelect")
                for (let i = 0; i < numberOfTeams; i++) {
                    if (
                        Number(mostRedCardTeamSelect.options[i].id) ===
                        Number(response.data.most_red_cards_team_prediction["team"])
                    ) {
                        mostRedCardTeamSelect.selectedIndex = i
                        break
                    }
                }
            }
            if (response.data.most_yellow_cards_team_prediction) {
                setMostYellowCardTeamPredictionId(response.data.most_yellow_cards_team_prediction["id"])
                let mostYellowCardTeamSelect = document.getElementById("mostYellowCardTeamSelect")
                for (let i = 0; i < numberOfTeams; i++) {
                    if (
                        Number(mostYellowCardTeamSelect.options[i].id) ===
                        Number(response.data.most_yellow_cards_team_prediction["team"])
                    ) {
                        mostYellowCardTeamSelect.selectedIndex = i
                        break
                    }
                }
            }
        })
    }

    function getPlayerIdAndPlayerName(selectId) {
        return {
            name: document.getElementById(selectId).options[document.getElementById(selectId).selectedIndex].value,
            playerId: document.getElementById(selectId).options[document.getElementById(selectId).selectedIndex].id,
        }
    }

    function getTeamIdAndTeamName(selectId) {
        return {
            name: document.getElementById(selectId).options[document.getElementById(selectId).selectedIndex].value,
            teamId: document.getElementById(selectId).options[document.getElementById(selectId).selectedIndex].id,
        }
    }

    function handleBestScorerButton() {
        const playerIdAndPlayerName = getPlayerIdAndPlayerName("bestScorerSelect")

        if (!bestScorerPredictionId) {
            GamesService.setMostScorerPrediction({
                player: playerIdAndPlayerName.playerId,
                user: AuthService.getUserId(),
            })
                .then(function (response) {
                    setInfoMessage("success", `${playerIdAndPlayerName.name} set as best scorer`)
                    setBestScorerPredictionId(response.data.id)
                })
                .catch((response) => {
                    if (response.response.data.name) {
                        setInfoMessage("error", `${response.response.data.name}`)
                    } else if (response.response.data.non_field_errors) {
                        setInfoMessage("error", `${response.response.data.non_field_errors}`)
                    } else {
                        setInfoMessage("error", `${response.message}`)
                    }
                })
        } else {
            GamesService.updateMostScorerPrediction(bestScorerPredictionId, {
                player: playerIdAndPlayerName.playerId,
                user: AuthService.getUserId(),
            })
                .then(function (response) {
                    setInfoMessage("success", `${playerIdAndPlayerName.name} updated as best scorer`)
                    setBestScorerPredictionId(response.data.id)
                })
                .catch((response) => {
                    if (response.response.data.name) {
                        setInfoMessage("error", `${response.response.data.name}`)
                    } else if (response.response.data.non_field_errors) {
                        setInfoMessage("error", `${response.response.data.non_field_errors}`)
                    } else {
                        setInfoMessage("error", `${response.message}`)
                    }
                })
        }
    }

    function handleBestAssistantButton() {
        const playerIdAndPlayerName = getPlayerIdAndPlayerName("bestAssistantSelect")

        if (!bestAssistantPredictionId) {
            GamesService.setMostAssistsPrediction({
                player: playerIdAndPlayerName.playerId,
                user: AuthService.getUserId(),
            })
                .then(function (response) {
                    setBestAssistantPredictionId(response.data.id)
                    setInfoMessage("success", `${playerIdAndPlayerName.name} set as best assistant`)
                })
                .catch((response) => {
                    if (response.response.data.name) {
                        setInfoMessage("error", `${response.response.data.name}`)
                    } else if (response.response.data.non_field_errors) {
                        setInfoMessage("error", `${response.response.data.non_field_errors}`)
                    } else {
                        setInfoMessage("error", `${response.message}`)
                    }
                })
        } else {
            GamesService.updateMostAssistsPrediction(bestAssistantPredictionId, {
                player: playerIdAndPlayerName.playerId,
                user: AuthService.getUserId(),
            })
                .then(function (response) {
                    setBestAssistantPredictionId(response.data.id)
                    setInfoMessage("success", `${playerIdAndPlayerName.name} updated as best assistant`)
                })
                .catch((response) => {
                    if (response.response.data.name) {
                        setInfoMessage("error", `${response.response.data.name}`)
                    } else if (response.response.data.non_field_errors) {
                        setInfoMessage("error", `${response.response.data.non_field_errors}`)
                    } else {
                        setInfoMessage("error", `${response.message}`)
                    }
                })
        }
    }

    function handleWorldCupWinnerButton() {
        const teamIdAndTeamName = getTeamIdAndTeamName("worldCupWinnerSelect")
        if (!worldCupWinnerPredictionId) {
            GamesService.setWorldCupWinnerPrediction({
                team: teamIdAndTeamName.teamId,
                user: AuthService.getUserId(),
            })
                .then(function (response) {
                    setWorldCupWinnerPredictionId(response.data.id)
                    setInfoMessage("success", `${teamIdAndTeamName.name} set as euro 2024 winner`)
                })
                .catch((response) => {
                    if (response.response.data.name) {
                        setInfoMessage("error", `${response.response.data.name}`)
                    } else if (response.response.data.non_field_errors) {
                        setInfoMessage("error", `${response.response.data.non_field_errors}`)
                    } else {
                        setInfoMessage("error", `${response.message}`)
                    }
                })
        } else {
            GamesService.updateWorldCupWinnerPrediction(worldCupWinnerPredictionId, {
                team: teamIdAndTeamName.teamId,
                user: AuthService.getUserId(),
            })
                .then(function (response) {
                    setWorldCupWinnerPredictionId(response.data.id)
                    setInfoMessage("success", `${teamIdAndTeamName.name} updated as euro 2024 winner`)
                })
                .catch((response) => {
                    if (response.response.data.name) {
                        setInfoMessage("error", `${response.response.data.name}`)
                    } else if (response.response.data.non_field_errors) {
                        setInfoMessage("error", `${response.response.data.non_field_errors}`)
                    } else {
                        setInfoMessage("error", `${response.message}`)
                    }
                })
        }
    }

    function handleMostRedCardPlayerButton() {
        const playerIdAndPlayerName = getPlayerIdAndPlayerName("mostRedCardPlayerSelect")
        if (!mostRedCardPlayerPredictionId) {
            GamesService.setMostRedCardPlayerPrediction({
                player: playerIdAndPlayerName.playerId,
                user: AuthService.getUserId(),
            })
                .then(function (response) {
                    setMostRedCardPlayerPredictionId(response.data.id)
                    setInfoMessage("success", `${playerIdAndPlayerName.name} set as most red card player`)
                })
                .catch((response) => {
                    if (response.response.data.name) {
                        setInfoMessage("error", `${response.response.data.name}`)
                    } else if (response.response.data.non_field_errors) {
                        setInfoMessage("error", `${response.response.data.non_field_errors}`)
                    } else {
                        setInfoMessage("error", `${response.message}`)
                    }
                })
        } else {
            GamesService.updateMostRedCardPlayerPrediction(mostRedCardPlayerPredictionId, {
                player: playerIdAndPlayerName.playerId,
                user: AuthService.getUserId(),
            })
                .then(function (response) {
                    setMostRedCardPlayerPredictionId(response.data.id)
                    setInfoMessage("success", `${playerIdAndPlayerName.name} updated as most red card player`)
                })
                .catch((response) => {
                    if (response.response.data.name) {
                        setInfoMessage("error", `${response.response.data.name}`)
                    } else if (response.response.data.non_field_errors) {
                        setInfoMessage("error", `${response.response.data.non_field_errors}`)
                    } else {
                        setInfoMessage("error", `${response.message}`)
                    }
                })
        }
    }

    function handleMostRedCardTeamButton() {
        const teamIdAndTeamName = getTeamIdAndTeamName("mostRedCardTeamSelect")
        if (!mostRedCardTeamPredictionId) {
            GamesService.setMostRedCardsTeamPrediction({
                team: teamIdAndTeamName.teamId,
                user: AuthService.getUserId(),
            })
                .then(function (response) {
                    setMostRedCardTeamPredictionId(response.data.id)
                    setInfoMessage("success", `${teamIdAndTeamName.name} set as most red card team`)
                })
                .catch((response) => {
                    if (response.response.data.name) {
                        setInfoMessage("error", `${response.response.data.name}`)
                    } else if (response.response.data.non_field_errors) {
                        setInfoMessage("error", `${response.response.data.non_field_errors}`)
                    } else {
                        setInfoMessage("error", `${response.message}`)
                    }
                })
        } else {
            GamesService.updateMostRedCardsTeamPrediction(mostRedCardTeamPredictionId, {
                team: teamIdAndTeamName.teamId,
                user: AuthService.getUserId(),
            })
                .then(function (response) {
                    setMostRedCardTeamPredictionId(response.data.id)
                    setInfoMessage("success", `${teamIdAndTeamName.name} updated as most red card team`)
                })
                .catch((response) => {
                    if (response.response.data.name) {
                        setInfoMessage("error", `${response.response.data.name}`)
                    } else if (response.response.data.non_field_errors) {
                        setInfoMessage("error", `${response.response.data.non_field_errors}`)
                    } else {
                        setInfoMessage("error", `${response.message}`)
                    }
                })
        }
    }

    function handleMostYellowCardPlayerButton() {
        const playerIdAndPlayerName = getPlayerIdAndPlayerName("mostYellowCardPlayerSelect")
        if (!mostYellowCardPlayerPredictionId) {
            GamesService.setMostYellowCardPlayerPrediction({
                player: playerIdAndPlayerName.playerId,
                user: AuthService.getUserId(),
            })
                .then(function (response) {
                    setMostYellowCardPlayerPredictionId(response.data.id)
                    setInfoMessage("success", `${playerIdAndPlayerName.name} set as most red card player`)
                })
                .catch((response) => {
                    if (response.response.data.name) {
                        setInfoMessage("error", `${response.response.data.name}`)
                    } else if (response.response.data.non_field_errors) {
                        setInfoMessage("error", `${response.response.data.non_field_errors}`)
                    } else {
                        setInfoMessage("error", `${response.message}`)
                    }
                })
        } else {
            GamesService.updateMostYellowCardPlayerPrediction(mostYellowCardPlayerPredictionId, {
                player: playerIdAndPlayerName.playerId,
                user: AuthService.getUserId(),
            })
                .then(function (response) {
                    setMostYellowCardPlayerPredictionId(response.data.id)
                    setInfoMessage("success", `${playerIdAndPlayerName.name} updated as most red card player`)
                })
                .catch((response) => {
                    if (response.response.data.name) {
                        setInfoMessage("error", `${response.response.data.name}`)
                    } else if (response.response.data.non_field_errors) {
                        setInfoMessage("error", `${response.response.data.non_field_errors}`)
                    } else {
                        setInfoMessage("error", `${response.message}`)
                    }
                })
        }
    }

    function handleMostYellowCardTeamButton() {
        const teamIdAndTeamName = getTeamIdAndTeamName("mostYellowCardTeamSelect")
        if (!mostYellowCardTeamPredictionId) {
            GamesService.setMostYellowCardsTeamPrediction({
                team: teamIdAndTeamName.teamId,
                user: AuthService.getUserId(),
            })
                .then(function (response) {
                    setMostYellowCardTeamPredictionId(response.data.id)
                    setInfoMessage("success", `${teamIdAndTeamName.name} set as most red card team`)
                })
                .catch((response) => {
                    if (response.response.data.name) {
                        setInfoMessage("error", `${response.response.data.name}`)
                    } else if (response.response.data.non_field_errors) {
                        setInfoMessage("error", `${response.response.data.non_field_errors}`)
                    } else {
                        setInfoMessage("error", `${response.message}`)
                    }
                })
        } else {
            GamesService.updateMostYellowCardsTeamPrediction(mostYellowCardTeamPredictionId, {
                team: teamIdAndTeamName.teamId,
                user: AuthService.getUserId(),
            })
                .then(function (response) {
                    setMostYellowCardTeamPredictionId(response.data.id)
                    setInfoMessage("success", `${teamIdAndTeamName.name} updated as most red card team`)
                })
                .catch((response) => {
                    if (response.response.data.name) {
                        setInfoMessage("error", `${response.response.data.name}`)
                    } else if (response.response.data.non_field_errors) {
                        setInfoMessage("error", `${response.response.data.non_field_errors}`)
                    } else {
                        setInfoMessage("error", `${response.message}`)
                    }
                })
        }
    }

    function handleAddPlayerButton() {
        GamesService.addPlayer({ name: player })
            .then(function (response) {
                updateSelect([response.data], "bestScorerSelect")
                updateSelect([response.data], "bestAssistantSelect")
                updateSelect([response.data], "mostRedCardPlayerSelect")
                updateSelect([response.data], "mostYellowCardPlayerSelect")
                document.getElementById("player-add-input").value = ""
            })
            .then(function (response) {
                setInfoMessage("success", `${player} added`)
            })
            .catch((response) => {
                if (response.response.data.name) {
                    setInfoMessage("error", `${response.response.data.name}`)
                } else if (response.response.data.non_field_errors) {
                    setInfoMessage("error", `${response.response.data.non_field_errors}`)
                } else {
                    setInfoMessage("error", `${response.message}`)
                }
            })
    }

    return (
        <div>
            <table className={"table table-striped individual-tipsters-table"}>
                <thead>
                    <tr>
                        <th scope="col">Description</th>
                        <th scope="col">Selection</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Best scorer </td>
                        <td>
                            <select
                                className={"form-select form-select-sm individual-select"}
                                id={"bestScorerSelect"}
                                name="bestScorer"
                                disabled={inputsDisabled}
                            >
                                <option>---------</option>
                            </select>
                        </td>
                        <td>
                            <button
                                className={"btn btn-light"}
                                onClick={handleBestScorerButton}
                                disabled={inputsDisabled}
                            >
                                Save
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Best Assistant</td>
                        <td>
                            <select
                                className={"form-select form-select-sm individual-select"}
                                id={"bestAssistantSelect"}
                                name="bestAssistant"
                                disabled={inputsDisabled}
                            >
                                <option>---------</option>
                            </select>
                        </td>
                        <td>
                            <button
                                className={"btn btn-light"}
                                onClick={handleBestAssistantButton}
                                disabled={inputsDisabled}
                            >
                                Save
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Euro 2024 Winner</td>
                        <td>
                            <select
                                className={"form-select form-select-sm individual-select"}
                                id={"worldCupWinnerSelect"}
                                name="worldCupWinner"
                                disabled={inputsDisabled}
                            >
                                <option>---------</option>
                            </select>
                        </td>
                        <td>
                            <button
                                className={"btn btn-light"}
                                onClick={handleWorldCupWinnerButton}
                                disabled={inputsDisabled}
                            >
                                Save
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Most red card player</td>
                        <td>
                            <select
                                className={"form-select form-select-sm individual-select"}
                                id={"mostRedCardPlayerSelect"}
                                name="mostRedCardPlayer"
                                disabled={inputsDisabled}
                            >
                                <option>---------</option>
                            </select>
                        </td>
                        <td>
                            <button
                                className={"btn btn-light"}
                                onClick={handleMostRedCardPlayerButton}
                                disabled={inputsDisabled}
                            >
                                Save
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Most red card team</td>
                        <td>
                            <select
                                className={"form-select form-select-sm individual-select"}
                                id={"mostRedCardTeamSelect"}
                                name="mostRedCardTeam"
                                disabled={inputsDisabled}
                            >
                                <option>---------</option>
                            </select>
                        </td>
                        <td>
                            <button
                                className={"btn btn-light"}
                                onClick={handleMostRedCardTeamButton}
                                disabled={inputsDisabled}
                            >
                                Save
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Most yellow card player</td>
                        <td>
                            <select
                                className={"form-select form-select-sm individual-select"}
                                id={"mostYellowCardPlayerSelect"}
                                name="mostYellowCardPlayer"
                                disabled={inputsDisabled}
                            >
                                <option>---------</option>
                            </select>
                        </td>
                        <td>
                            <button
                                className={"btn btn-light"}
                                onClick={handleMostYellowCardPlayerButton}
                                disabled={inputsDisabled}
                            >
                                Save
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Most yellow card team</td>
                        <td>
                            <select
                                className={"form-select form-select-sm individual-select"}
                                id={"mostYellowCardTeamSelect"}
                                name="mostYellowCardTeam"
                                disabled={inputsDisabled}
                            >
                                <option>---------</option>
                            </select>
                        </td>
                        <td>
                            <button
                                className={"btn btn-light"}
                                onClick={handleMostYellowCardTeamButton}
                                disabled={inputsDisabled}
                            >
                                Save
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Your player is missing?</td>
                        <td>
                            <input
                                className={"player-input"}
                                id={"player-add-input"}
                                onChange={(event) => {
                                    setPlayer(event.target.value)
                                }}
                                disabled={inputsDisabled}
                            />
                        </td>
                        <td>
                            <button
                                className={"btn btn-light"}
                                onClick={handleAddPlayerButton}
                                disabled={inputsDisabled}
                            >
                                Add
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}
