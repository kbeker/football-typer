import CustomContainer from "../commons/customContainer"
import "./registration.component.css"
import { useState } from "react"
import AuthService from "../services/authorization.service"
import { useNavigate } from "react-router-dom"
import { FrontendUrls } from "../constants"
import { setInfoMessage } from "../commons/helpers"

export default function RegistrationComponent(props) {
    const [email, setEmail] = useState()
    const [password, setPassword] = useState()
    const [password2, setPassword2] = useState()
    const [firstName, setFirstName] = useState()
    const [lastName, setLastName] = useState()
    const navigate = useNavigate()

    function onClickInfoAuthDiv() {
        let infoDiv = document.getElementById("info-auth-div")
        infoDiv.classList.add("disabled")
    }

    function handleRegistration(event) {
        event.preventDefault()
        AuthService.register({
            email: email,
            first_name: firstName,
            last_name: lastName,
            password: password,
            password2: password2,
        })
            .then(function (success) {
                if (success) {
                    navigate("/")
                }
            })
            .catch((response) => {
                if (response.response.data.detail) {
                    setInfoMessage("error", `${response.response.data.detail}`)
                } else if (response.response.data) {
                    let errors = ""
                    for (const property in response.response.data) {
                        errors += `${response.response.data[property]} <br/>`
                    }
                    setInfoMessage("error", `${errors}`)
                } else if (response.response.data.name) {
                    setInfoMessage("error", `${response.response.data.name}`)
                } else if (response.response.data.non_field_errors) {
                    setInfoMessage("error", `${response.response.data.non_field_errors}`)
                } else {
                    setInfoMessage("error", `${response.message}`)
                }
            })
    }

    return (
        <div className={"registration-container"}>
            <div id={"info-auth-div"} className={"info-auth-div disabled"} onClick={onClickInfoAuthDiv} />
            <CustomContainer
                containerName={<div className={"container-details"}>Registration</div>}
                containerBody={
                    <div className={"container-body"}>
                        <div className={"sign-up-form"}>
                            <form className="form" onSubmit={handleRegistration}>
                                <input
                                    className="input"
                                    placeholder="Email"
                                    type="text"
                                    onChange={(event) => setEmail(event.target.value.toLowerCase())}
                                />
                                <input
                                    className="input"
                                    placeholder="First Name"
                                    type="text"
                                    onChange={(event) => setFirstName(event.target.value)}
                                />
                                <input
                                    className="input"
                                    placeholder="Last Name"
                                    type="text"
                                    onChange={(event) => setLastName(event.target.value)}
                                />
                                <input
                                    className="input"
                                    placeholder="Password"
                                    type="password"
                                    onChange={(event) => setPassword(event.target.value)}
                                />
                                <input
                                    className="input"
                                    placeholder="Repeat password"
                                    type="password"
                                    onChange={(event) => setPassword2(event.target.value)}
                                />
                                <input className="confirm-button" type="submit" value="Register" />
                            </form>
                        </div>
                    </div>
                }
                containerBottom={<div>If you want to login use {<a href={FrontendUrls.login}>this link</a>}</div>}
            />
        </div>
    )
}
