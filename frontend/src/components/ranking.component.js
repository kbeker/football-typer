import GamesService from "../services/games.service"
import { useEffect, useState } from "react"
import RankingElement from "./rankingElement.component"

export default function RankingComponent() {
    const [data, setData] = useState()

    function fetchRanking() {
        GamesService.getRanking().then(function (response) {
            console.log(response.data)
            setData(response.data)
        })
    }

    useEffect(() => {
        fetchRanking()
    }, [])

    return (
        <div className={"ranking-container"}>
            <table className="table table-striped games-table">
                <thead>
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Name</th>
                        <th scope="col">Points</th>
                    </tr>
                </thead>
                <tbody id={"ranking-table-body"}>
                    {data &&
                        data.sort((a, b) => (a.points < b.points ? 1 : -1)) &&
                        data.map(({ id, user, first_name, last_name, points }, index) => (
                            <RankingElement
                                key={id}
                                rankingId={id}
                                userId={user}
                                rankingNumber={index + 1}
                                firstName={first_name}
                                lastName={last_name}
                                points={points}
                            />
                        ))}
                </tbody>
            </table>
        </div>
    )
}
