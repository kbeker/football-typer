import "./sidebarElement.component.css"
import { Link, useLocation } from "react-router-dom"
import React from "react"

export default function SidebarElementComponent({ to, toName, icon, onClickAction }) {
    const { pathname } = useLocation()

    return (
        <ul onClick={onClickAction !== undefined ? onClickAction : undefined} className="sidebar-element">
            <Link to={`${to}`} className={pathname === to ? "active" : undefined}>
                {icon} <span className={"text"}> {toName}</span>
            </Link>
        </ul>
    )
}
