import "./main.component.css"
import SidebarComponent from "./sidebar.component"
import Spinner from "../commons/Spinner"
import NotFoundComponent from "./notFound.component"
import { Route, Routes } from "react-router-dom"
import GamesComponent from "./games.component"
import { FrontendUrls } from "../constants"
import RankingComponent from "./ranking.component"

export default function MainComponent(props) {
    return (
        <div className={"main-container"}>
            <div className={"navigation-bar"}>
                <SidebarComponent />
            </div>
            <div id={"content-container"} className={"content-container"}>
                <Spinner />
                <Routes>
                    <Route path={FrontendUrls.games} element={<GamesComponent />} />
                    <Route path={FrontendUrls.ranking} element={<RankingComponent />} />
                    <Route path="*" element={<NotFoundComponent />} />
                </Routes>
            </div>
        </div>
    )
}
