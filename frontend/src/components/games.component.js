import "./games.components.css"
import SingleGameComponent from "./singleGame.component"
import GamesService from "../services/games.service"
import { useEffect, useState } from "react"
import IndividualTipsterComponent from "./individualTipster.component"

export default function GamesComponent(props) {
    const [data, setData] = useState()

    function fetchGames() {
        GamesService.getGames().then(function (response) {
            setData(response.data)
        })
    }

    function onClickInfoDiv() {
        let infoDiv = document.getElementById("info-div")
        infoDiv.classList.add("disabled")
    }

    useEffect(() => {
        fetchGames()
    }, [])

    return (
        <div className={"game-container"}>
            <div id={"info-div"} className={"info-div disabled"} onClick={onClickInfoDiv} />
            <table className="table table-striped games-table">
                <thead>
                    <tr>
                        <th scope="col">Date</th>
                        <th scope="col">Game and Result</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {data &&
                        data.sort((a, b) => (a.date < b.date ? 1 : -1)) &&
                        data.map(({ id, team_1, team_2, date, team_1_goals, team_2_goals }) => (
                            <SingleGameComponent key={id} id={id} team1={team_1} team2={team_2} date={date} />
                        ))}
                </tbody>
            </table>
            <div className={"individual-tipsters-container"}>
                <IndividualTipsterComponent />
            </div>
        </div>
    )
}
