import "./singleGame.component.css"
import { useEffect, useState } from "react"
import GamesService from "../services/games.service"
import AuthService from "../services/authorization.service"
import { setInfoMessage } from "../helpers"

export default function SingleGameComponent({ id, team1, team2, date, team_1_goals, team_2_goals }) {
    const [team1Goals, setTeam1Goals] = useState()
    const [team2Goals, setTeam2Goals] = useState()
    const [predictedGameId, setPredictedGameId] = useState()
    const [disabledGame, setDisabledGame] = useState(false)

    let gameDate = new Date(date)
    function roundTo2Digits(variable) {
        return String(variable).padStart(2, "0")
    }

    function constructDate(date) {
        return `${roundTo2Digits(date.getDate())}/${roundTo2Digits(
            date.getMonth() + 1
        )}/${date.getFullYear()} ${roundTo2Digits(date.getHours())}:${roundTo2Digits(date.getMinutes())}`
    }

    function handleSave() {
        if (predictedGameId) {
            GamesService.updatePredictGame(predictedGameId, {
                game: id,
                user: AuthService.getUserId(),
                team_1_goals: team1Goals,
                team_2_goals: team2Goals,
                winner: document.getElementById(`${id}-select`).value,
            })
                .then(function (response) {
                    setPredictedGameId(response.data.id)
                    setInfoMessage("success", `Prediction for the game ${team1} vs ${team2} successfully updated`)
                })
                .catch((response) => {
                    if (response.response.data.name) {
                        setInfoMessage("error", `${response.response.data.name}`)
                    } else if (response.response.data.non_field_errors) {
                        setInfoMessage("error", `${response.response.data.non_field_errors}`)
                    } else {
                        setInfoMessage("error", `${response.message}`)
                    }
                })
        } else {
            GamesService.predictGame({
                game: id,
                user: AuthService.getUserId(),
                team_1_goals: team1Goals,
                team_2_goals: team2Goals,
                winner: document.getElementById(`${id}-select`).value,
            })
                .then(function (response) {
                    setPredictedGameId(response.data.id)
                    setInfoMessage("success", `Prediction for the game ${team1} vs ${team2} successfully saved`)
                })
                .catch((response) => {
                    if (response.response.data.name) {
                        setInfoMessage("error", `${response.response.data.name}`)
                    } else if (response.response.data.non_field_errors) {
                        setInfoMessage("error", `${response.response.data.non_field_errors}`)
                    } else {
                        setInfoMessage("error", `${response.message}`)
                    }
                })
        }
    }

    useEffect(() => {
        fetchPredictedGame()
        if (Date.now() > new Date(date)) {
            setDisabledGame(true)
        }
    }, [])

    function fetchPredictedGame() {
        GamesService.getPredictedGameByGameId(id)
            .then((response) => {
                setPredictedGameId(response.data.id)
                document.getElementById(`${id}-team1`).value = response.data.team_1_goals
                document.getElementById(`${id}-team2`).value = response.data.team_2_goals
                let select = document.getElementById(`${id}-select`)
                for (let i = 0; i <= 3; i++) {
                    if (select.options[i].value === response.data.winner) {
                        select.selectedIndex = i
                        break
                    }
                }
            })
            .catch((response) => {})
    }

    return (
        <tr>
            <td>{constructDate(gameDate)}</td>
            <td className={"game-column"}>
                <div className={"input-goals-group"}>
                    <div className="input-group input-group-sm mb-4">
                        <div className="input-group-prepend">
                            <span className="input-group-text" id="inputGroup-sizing-sm">
                                {team1}
                            </span>
                        </div>
                        <input
                            id={`${id}-team1`}
                            type="text"
                            className={"form-control left-goal-input"}
                            aria-label="Small"
                            aria-describedby="inputGroup-sizing-sm"
                            onChange={(event) => {
                                setTeam1Goals(event.target.value)
                            }}
                            disabled={disabledGame}
                        />
                    </div>
                    <div className="input-group input-group-sm mb-4">
                        <input
                            id={`${id}-team2`}
                            type="text"
                            className={"form-control right-goal-input"}
                            aria-label="Small"
                            aria-describedby="inputGroup-sizing-sm"
                            onChange={(event) => {
                                setTeam2Goals(event.target.value)
                            }}
                            disabled={disabledGame}
                        />
                        <div className="input-group-prepend">
                            <span className="input-group-text" id="inputGroup-sizing-sm">
                                {team2}
                            </span>
                        </div>
                    </div>
                </div>
                <select
                    className={"form-select form-select-sm winner-select"}
                    name="results"
                    id={`${id}-select`}
                    disabled={disabledGame}
                    defaultValue={"---------"}
                >
                    <option>---------</option>
                    <option key={"team_1_winner"} value="team_1_winner">
                        {team1}
                    </option>
                    <option key={"draw"} value="draw">
                        Draw
                    </option>
                    <option key={"team_2_winner"} value="team_2_winner">
                        {team2}
                    </option>
                </select>
            </td>
            <td>
                <button className={"btn btn-light"} onClick={handleSave} disabled={disabledGame}>
                    Save
                </button>
            </td>
        </tr>
    )
}
