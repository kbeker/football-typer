export default function RankingElement({ rankingId, userId, rankingNumber, firstName, lastName, points }) {
    return (
        <tr>
            <td>{rankingNumber}</td>
            <td>{`${firstName} ${lastName}`}</td>
            <td>{points}</td>
        </tr>
    )
}
