import "./App.css"
import LoginComponent from "./components/login.component"
import RegistrationComponent from "./components/registration.component"
import { Route, Routes, useLocation, useNavigate } from "react-router-dom"
import { useEffect } from "react"
import MainComponent from "./components/main.component"
import { FrontendUrls } from "./constants"

function App() {
    const accessToken = sessionStorage.getItem("accessToken")
    const navigate = useNavigate()
    const { pathname } = useLocation()

    useEffect(() => {
        // add token verification call if it's not null
        if (accessToken == null && FrontendUrls.registration !== pathname) {
            // add redirections to other pages if user
            // wants to go but is not logged in
            navigate(FrontendUrls.login)
        } else if (
            accessToken !== null &&
            (pathname.endsWith(FrontendUrls.registration) || pathname.endsWith(FrontendUrls.login))
        ) {
            navigate(FrontendUrls.games)
        } else {
            navigate(pathname)
        }
    }, [accessToken, pathname, navigate])

    return (
        <Routes>
            <Route path="*" element={<MainComponent />} />
            <Route exact path={FrontendUrls.login} element={<LoginComponent />} />
            <Route exact path={FrontendUrls.registration} element={<RegistrationComponent />} />
        </Routes>
    )
}

export default App
