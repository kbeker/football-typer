const AppConfig = {
    BackendUrl: getBackendUrl(),
}

function getBackendUrl() {
    if (window._env && window._env.BACKEND_URL) {
        return window._env.BACKEND_URL
    }
    return "http://localhost:8000"
}

export default AppConfig
