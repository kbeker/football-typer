import "./customContainer.css"
import "./helpers.css"

export default function CustomContainer({ containerName, containerBody, containerBottom }) {
    return (
        <div className="outer-container">
            <div className="inner-container">
                <div className="container-name">{containerName}</div>
                <div className="container-body">{containerBody}</div>
                <div className="container-bottom">{containerBottom}</div>
            </div>
        </div>
    )
}
