function disableInfoAuthDiv() {
    const infoAuthDiv = document.getElementById("info-auth-div")
    infoAuthDiv.classList.add("disabled")
    infoAuthDiv.classList.remove("error-info")
    infoAuthDiv.classList.remove("success-info")
}

export function setInfoMessage(messageType, message) {
    clearTimeout(localStorage.getItem("timeoutId"))
    disableInfoAuthDiv()
    let infoAuthDiv = document.getElementById("info-auth-div")
    infoAuthDiv.classList.remove("disabled")
    if (messageType === "success") {
        infoAuthDiv.classList.add("success-info")
    } else {
        infoAuthDiv.classList.add("error-info")
    }
    infoAuthDiv.innerHTML = message
    const timeoutId = setTimeout(() => {
        disableInfoAuthDiv()
    }, 2000)
    localStorage.setItem("timeoutId", timeoutId)
}
