import "./spinner.css"

const Spinner = () => <div id="loading-spinner" className="loading-spinner disabled" />

export default Spinner
