import { BackendEndpointUrls, FrontendUrls, Tokens } from "./constants"
import axios from "axios"
import AuthService from "./services/authorization.service"

export function redirectLogin() {
    window.location = FrontendUrls.login
}

export function configureAxios() {
    axios.interceptors.request.use(async (request) => {
        if (
            !AuthService.isAccessTokenValid() &&
            !request.url.endsWith(BackendEndpointUrls.obtainToken) &&
            !request.url.endsWith(BackendEndpointUrls.register)
        ) {
            if (!AuthService.isRefreshTokenValid()) {
                redirectLogin()
            } else {
                if (!request.url.endsWith(BackendEndpointUrls.refreshToken)) {
                    await AuthService.refreshToken()
                        .then(function (response) {
                            if (response.status !== 200) {
                                redirectLogin()
                            }
                        })
                        .catch(function (error) {
                            redirectLogin()
                        })
                }
            }
        }
        request.headers["AUTHORIZATION"] = `Bearer ${window.sessionStorage.getItem(Tokens.access)}`
        return request
    })
}

function disableInfoDiv() {
    const infoDiv = document.getElementById("info-div")
    infoDiv.classList.add("disabled")
    infoDiv.classList.remove("error-info")
    infoDiv.classList.remove("success-info")
}

export function setInfoMessage(messageType, message) {
    clearTimeout(localStorage.getItem("timeoutId"))
    disableInfoDiv()
    let infoDiv = document.getElementById("info-div")
    infoDiv.classList.remove("disabled")
    if (messageType === "success") {
        infoDiv.classList.add("success-info")
    } else {
        infoDiv.classList.add("error-info")
    }
    infoDiv.innerHTML = message
    const timeoutId = setTimeout(() => {
        disableInfoDiv()
    }, 2000)
    localStorage.setItem("timeoutId", timeoutId)
}

export function overlayContent() {
    let contentWindow = document.getElementById("content-container")
    contentWindow.classList.add("overlay")
    let loadingSpinner = document.getElementById("loading-spinner")
    loadingSpinner.classList.remove("disabled")
}

export function deleteOverlayContent() {
    let contentWindow = document.getElementById("content-container")
    contentWindow.classList.remove("overlay", "overlay-opacity")
    let loadingSpinner = document.getElementById("loading-spinner")
    loadingSpinner.classList.add("disabled")
}
