import axios from "axios"
import { BackendEndpointUrls, Tokens } from "../constants"
import AppConfig from "../config"

const GamesService = {
    getGames: async function () {
        return await axios.get(`${AppConfig.BackendUrl}${BackendEndpointUrls.games}`)
    },
    predictGame: async function (body) {
        return await axios.post(`${AppConfig.BackendUrl}${BackendEndpointUrls.gamePredictions}`, body)
    },
    updatePredictGame: async function (predictGameId, body) {
        return await axios.patch(`${AppConfig.BackendUrl}${BackendEndpointUrls.gamePredictions}${predictGameId}/`, body)
    },
    getPredictedGame: async function (id) {
        return await axios.get(`${AppConfig.BackendUrl}${BackendEndpointUrls.gamePredictions}${id}/`)
    },
    getPredictedGameByGameId: async function (id) {
        return await axios.get(`${AppConfig.BackendUrl}${BackendEndpointUrls.gamePredictionsByGameId}${id}/`)
    },
    getPlayers: async function () {
        return await axios.get(`${AppConfig.BackendUrl}${BackendEndpointUrls.players}`)
    },
    getTeams: async function () {
        return await axios.get(`${AppConfig.BackendUrl}${BackendEndpointUrls.teams}`)
    },
    setMostScorerPrediction: async function (body) {
        return await axios.post(`${AppConfig.BackendUrl}${BackendEndpointUrls.mostScorerPrediction}`, body)
    },
    updateMostScorerPrediction: async function (id, body) {
        return await axios.patch(`${AppConfig.BackendUrl}${BackendEndpointUrls.mostScorerPrediction}${id}/`, body)
    },
    setMostAssistsPrediction: async function (body) {
        return await axios.post(`${AppConfig.BackendUrl}${BackendEndpointUrls.mostAssistsPrediction}`, body)
    },
    updateMostAssistsPrediction: async function (id, body) {
        return await axios.patch(`${AppConfig.BackendUrl}${BackendEndpointUrls.mostAssistsPrediction}${id}/`, body)
    },
    setMostRedCardPlayerPrediction: async function (body) {
        return await axios.post(`${AppConfig.BackendUrl}${BackendEndpointUrls.mostRedCardPlayerPrediction}`, body)
    },
    setMostYellowCardPlayerPrediction: async function (body) {
        return await axios.post(`${AppConfig.BackendUrl}${BackendEndpointUrls.mostYellowCardPlayerPrediction}`, body)
    },
    updateMostRedCardPlayerPrediction: async function (id, body) {
        return await axios.patch(
            `${AppConfig.BackendUrl}${BackendEndpointUrls.mostRedCardPlayerPrediction}${id}/`,
            body
        )
    },
    updateMostYellowCardPlayerPrediction: async function (id, body) {
        return await axios.patch(
            `${AppConfig.BackendUrl}${BackendEndpointUrls.mostYellowCardPlayerPrediction}${id}/`,
            body
        )
    },
    setWorldCupWinnerPrediction: async function (body) {
        return await axios.post(`${AppConfig.BackendUrl}${BackendEndpointUrls.worldCupWinnerPrediction}`, body)
    },
    updateWorldCupWinnerPrediction: async function (id, body) {
        return await axios.patch(`${AppConfig.BackendUrl}${BackendEndpointUrls.worldCupWinnerPrediction}${id}/`, body)
    },
    setMostRedCardsTeamPrediction: async function (body) {
        return await axios.post(`${AppConfig.BackendUrl}${BackendEndpointUrls.mostRedCardsTeamPrediction}`, body)
    },
    setMostYellowCardsTeamPrediction: async function (body) {
        return await axios.post(`${AppConfig.BackendUrl}${BackendEndpointUrls.mostYellowCardsTeamPrediction}`, body)
    },
    updateMostRedCardsTeamPrediction: async function (id, body) {
        return await axios.patch(`${AppConfig.BackendUrl}${BackendEndpointUrls.mostRedCardsTeamPrediction}${id}/`, body)
    },
    updateMostYellowCardsTeamPrediction: async function (id, body) {
        return await axios.patch(
            `${AppConfig.BackendUrl}${BackendEndpointUrls.mostYellowCardsTeamPrediction}${id}/`,
            body
        )
    },
    getUserPredictions: async function (id) {
        return await axios.get(`${AppConfig.BackendUrl}${BackendEndpointUrls.userPredictionsByUserId}${id}/`)
    },
    addPlayer: async function (body) {
        return await axios.post(`${AppConfig.BackendUrl}${BackendEndpointUrls.players}`, body)
    },
    getFirstGameDateTime: async function () {
        return await axios.get(`${AppConfig.BackendUrl}${BackendEndpointUrls.firstGameDateTime}`)
    },
    getRanking: async function () {
        return await axios.get(`${AppConfig.BackendUrl}${BackendEndpointUrls.ranking}`)
    },
    getConfig: async function () {
        return await axios.get(`${AppConfig.BackendUrl}${BackendEndpointUrls.config}`)
    },
}

export default GamesService
