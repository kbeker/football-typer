import axios from "axios"
import { BackendEndpointUrls, Tokens } from "../constants"
import AppConfig from "../config"
import { redirectLogin } from "../helpers"

const AuthService = {
    register: async function (body) {
        return await axios
            .post(`${AppConfig.BackendUrl}${BackendEndpointUrls.register}`, body)
            .then(function (response) {
                if (response.status === 201) {
                    redirectLogin()
                    return true
                } else {
                    // add handling different status codes
                    return false
                }
            })
    },

    obtainToken: async function (body) {
        // move request to logic to separate service
        return await axios
            .post(`${AppConfig.BackendUrl}${BackendEndpointUrls.obtainToken}`, body)
            .then(function (response) {
                if (response.status === 200) {
                    window.sessionStorage.setItem(Tokens.access, response.data.access)
                    window.sessionStorage.setItem(Tokens.refresh, response.data.refresh)
                    return true
                } else {
                    // add handling different status codes
                    return false
                }
            })
    },
    refreshToken: async function () {
        return await axios
            .post(`${AppConfig.BackendUrl}${BackendEndpointUrls.refreshToken}`, {
                refresh: sessionStorage.getItem("refreshToken"),
            })
            .then(function (response) {
                if (response.status === 200) {
                    window.sessionStorage.setItem("accessToken", response.data.access)
                    return response
                }
            })
    },
    isRefreshTokenValid: function () {
        return isTokenValid(window.sessionStorage.getItem(Tokens.refresh))
    },

    isAccessTokenValid: function () {
        return isTokenValid(window.sessionStorage.getItem(Tokens.access))
    },
    getUserId: function () {
        try {
            return parseJwt(window.sessionStorage.getItem(Tokens.access)).user_id
        } catch {
            redirectLogin()
        }
    },
}

function isTokenValid(token, threshold = 0) {
    let parsedJwt
    try {
        parsedJwt = parseJwt(token)
    } catch {
        return false
    }
    const expirationTime = parsedJwt.exp
    const currentTimestamp = Math.floor(new Date().getTime() / 1000)
    return currentTimestamp <= expirationTime + threshold
}

function parseJwt(token) {
    const base64Url = token.split(".")[1]
    const base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/")
    const jsonPayload = decodeURIComponent(
        window
            .atob(base64)
            .split("")
            .map(function (c) {
                return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2)
            })
            .join("")
    )

    return JSON.parse(jsonPayload)
}

export default AuthService
