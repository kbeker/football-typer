import axios from "axios"
import AppConfig from "../config"
import { BackendEndpointUrls } from "../constants"

const UserService = {
    getUser: async function (userId) {
        return await axios.get(`${AppConfig.BackendUrl}${BackendEndpointUrls.user}${userId}/`)
    },
}

export default UserService
