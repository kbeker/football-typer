const refreshTokenName = "refreshToken"
const accessTokenName = "accessToken"

window.addEventListener(
    "storage",
    (event) => {
        if (event.storageArea === localStorage) {
            if (event.key === "DELETE_TOKENS") {
                sessionStorage.removeItem(refreshTokenName)
                sessionStorage.removeItem(accessTokenName)
                localStorage.removeItem("DELETE_TOKENS")
                window.location = "/"
            }
            let refreshToken = sessionStorage.getItem(refreshTokenName)
            let accessToken = sessionStorage.getItem(accessTokenName)

            if (event.key === "REQUESTING_TOKENS" && event.newValue && refreshToken && accessToken) {
                localStorage.setItem("ACCESS_TOKEN_SHARING", accessToken)
                localStorage.setItem("REFRESH_TOKEN_SHARING", refreshToken)
                localStorage.removeItem("ACCESS_TOKEN_SHARING")
                localStorage.removeItem("REFRESH_TOKEN_SHARING")
            }

            if (event.key === "REFRESH_TOKEN_SHARING" && !refreshToken) {
                sessionStorage.setItem(refreshTokenName, event.newValue)
            }

            if (event.key === "ACCESS_TOKEN_SHARING" && !accessToken) {
                sessionStorage.setItem(accessTokenName, event.newValue)
            }
        }
    },
    false
)

localStorage.setItem("REQUESTING_TOKENS", Date.now().toString())
localStorage.removeItem("REQUESTING_TOKENS")
