#!/bin/bash

docker exec pg-13 bash -c 'psql -U postgres --command="drop database football_typer;"'
docker exec pg-13 bash -c 'psql -U postgres --command="create database football_typer;"'

./manage.py makemigrations
./manage.py migrate
DJANGO_SUPERUSER_USERNAME=admin \
DJANGO_SUPERUSER_PASSWORD=password \
DJANGO_SUPERUSER_EMAIL="admin@admin.com" \
python manage.py createsuperuser --noinput
