from .dev import *  # noqa: F403, F401 pylint: disable=wildcard-import

DATABASES["default"]["USER"] = "postgres"
DATABASES["default"]["PASSWORD"] = "docker"
DATABASES["default"]["HOST"] = "localhost"
DATABASES["default"]["PORT"] = "5013"
