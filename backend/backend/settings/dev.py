from .defaults import *  # noqa: F403, F401 pylint: disable=wildcard-import

DEBUG = True

SECRET_KEY = "secret-key"

DATABASES["default"]["USER"] = "postgres"
DATABASES["default"]["PASSWORD"] = ""
DATABASES["default"]["HOST"] = ""
DATABASES["default"]["PORT"] = ""

ALLOWED_HOSTS = ["*"]
