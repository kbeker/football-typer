from typing import Any

from django.db.models import Model as DjangoModel


class Model(DjangoModel):
    class Meta:
        abstract = True

    def save(
        self, force_insert: bool = False, force_update: bool = False, using: Any = None, update_fields: Any = None
    ) -> None:
        self.full_clean()
        super().save(force_insert, force_update, using, update_fields)
