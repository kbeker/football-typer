#!/bin/bash -e

pipenv run python manage.py migrate
pipenv run python manage.py collectstatic --noinput --clear --force-color --verbosity 0

pipenv run gunicorn backend.wsgi:application --workers 5 --log-level=info --timeout 150 --bind 0.0.0.0:8000
