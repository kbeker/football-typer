from typing import Dict

from constance import config
from django.utils.timezone import now
from rest_framework.exceptions import ValidationError
from rest_framework.fields import ChoiceField
from rest_framework.fields import IntegerField
from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.serializers import ModelSerializer

from tipster.models import RESULT_TYPES
from tipster.models import Game
from tipster.models import GamePrediction
from tipster.models import MostAssistsPrediction
from tipster.models import MostRedCardsPlayerPrediction
from tipster.models import MostRedCardsTeamPrediction
from tipster.models import MostScorerPrediction
from tipster.models import MostYellowCardsPlayerPrediction
from tipster.models import MostYellowCardsTeamPrediction
from tipster.models import Player
from tipster.models import Team
from tipster.models import WorldCupWinnerPrediction
from users.models import CustomUser


class PlayerSerializer(ModelSerializer):
    class Meta:
        model = Player
        fields = ["id", "name"]

    def validate(self, attrs):
        if games := Game.objects.order_by("date"):
            first_game = games.first()
            if now() > first_game.date and config.ALLOW_OUTDATED_INDIVIDUAL_PREDICTIONS is False:
                raise ValidationError("First game already started.")
        return attrs


class TeamSerializer(ModelSerializer):
    class Meta:
        model = Team
        fields = ["id", "name"]
        read_only_fields = ["id", "name"]


class GamePredictionSerializer(ModelSerializer):
    class Meta:
        model = GamePrediction
        fields = ["id", "game", "user", "team_1_goals", "team_2_goals", "winner"]

    game = PrimaryKeyRelatedField(queryset=Game.objects.all())
    user = PrimaryKeyRelatedField(queryset=CustomUser.objects.all())
    team_1_goals = IntegerField(min_value=0)
    team_2_goals = IntegerField(min_value=0)
    winner = ChoiceField(choices=RESULT_TYPES)

    def create(self, validated_data: Dict) -> GamePrediction:
        game_prediction = GamePrediction.objects.create(**validated_data)
        return game_prediction


class MostScorerPredictionSerializer(ModelSerializer):
    class Meta:
        model = MostScorerPrediction
        fields = ["id", "player", "user"]


class MostAssistsPredictionSerializer(ModelSerializer):
    class Meta:
        model = MostAssistsPrediction
        fields = ["id", "player", "user"]


class MostCardPlayerPredictionSerializerBase(ModelSerializer):
    class Meta:
        fields = ["id", "player", "user"]


class MostRedCardsPlayerPredictionSerializer(MostCardPlayerPredictionSerializerBase):
    class Meta(MostCardPlayerPredictionSerializerBase.Meta):
        model = MostRedCardsPlayerPrediction


class MostYellowCardsPlayerPredictionSerializer(MostCardPlayerPredictionSerializerBase):
    class Meta(MostCardPlayerPredictionSerializerBase.Meta):
        model = MostYellowCardsPlayerPrediction


class WorldCupWinnerPredictionSerializer(ModelSerializer):
    class Meta:
        model = WorldCupWinnerPrediction
        fields = ["id", "team", "user"]


class MostCardsTeamPredictionSerializerBase(ModelSerializer):
    class Meta:
        fields = ["id", "team", "user"]


class MostRedCardsTeamPredictionSerializer(MostCardsTeamPredictionSerializerBase):
    class Meta(MostCardsTeamPredictionSerializerBase.Meta):
        model = MostRedCardsTeamPrediction


class MostYellowCardsTeamPredictionSerializer(MostCardsTeamPredictionSerializerBase):
    class Meta(MostCardsTeamPredictionSerializerBase.Meta):
        model = MostYellowCardsTeamPrediction
