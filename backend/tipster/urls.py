from django.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter

from tipster.views import ConfigView
from tipster.views import CustomUserPredictions
from tipster.views import FirstGameDateTimeView
from tipster.views import GamePredictionByGameIdView
from tipster.views import GamePredictionViewSet
from tipster.views import GamesView
from tipster.views import MostAssistsPredictionViewSet
from tipster.views import MostRedCardsPlayerPredictionViewSet
from tipster.views import MostRedCardsTeamPredictionViewSet
from tipster.views import MostScorerPredictionViewSet
from tipster.views import MostYellowCardsPlayerPredictionViewSet
from tipster.views import MostYellowCardsTeamPredictionViewSet
from tipster.views import PlayerViewSet
from tipster.views import TeamsView
from tipster.views import WorldCupWinnerPredictionViewSet

router = DefaultRouter()
router.register("", GamePredictionViewSet, basename="game_prediction")

new_router = DefaultRouter()
new_router.register("most-scorer-prediction", MostScorerPredictionViewSet, basename="most_scorer_prediction")
new_router.register("most-assists-prediction", MostAssistsPredictionViewSet, basename="most_assists_prediction")
new_router.register(
    "most-red-cards-player-prediction", MostRedCardsPlayerPredictionViewSet, basename="most_red_cards_player_prediction"
)
new_router.register(
    "most-yellow-cards-player-prediction",
    MostYellowCardsPlayerPredictionViewSet,
    basename="most_yellow_cards_player_prediction",
)
new_router.register(
    "world-cup-winner-prediction", WorldCupWinnerPredictionViewSet, basename="world_cup_winner_prediction"
)
new_router.register(
    "most-red-cards-team-prediction", MostRedCardsTeamPredictionViewSet, basename="most_red_cards_team_prediction"
)
new_router.register(
    "most-yellow-cards-team-prediction",
    MostYellowCardsTeamPredictionViewSet,
    basename="most_yellow_cards_team_prediction",
)
new_router.register("players", PlayerViewSet, basename="player")

urlpatterns = [
    path("teams/", TeamsView.as_view(), name="teams"),
    path("games/", GamesView.as_view(), name="games"),
    path(
        "game-prediction-by-game-id/<int:id>/", GamePredictionByGameIdView.as_view(), name="game-prediction-by-game-id"
    ),
    path("user-predictions-by-user-id/<int:id>/", CustomUserPredictions.as_view(), name="user-prediction-by-user-id"),
    path("config/", ConfigView.as_view(), name="config"),
    path("game-prediction/", include(router.urls)),
    path("first-game-date-time/", FirstGameDateTimeView.as_view(), name="first-game-date-time"),
    path("", include(new_router.urls)),
]
