from django.contrib import admin
from django.contrib.admin import ModelAdmin

from tipster.models import Game
from tipster.models import GamePrediction
from tipster.models import MostAssists
from tipster.models import MostAssistsPrediction
from tipster.models import MostRedCardsPlayer
from tipster.models import MostRedCardsPlayerPrediction
from tipster.models import MostRedCardsTeam
from tipster.models import MostRedCardsTeamPrediction
from tipster.models import MostScorer
from tipster.models import MostScorerPrediction
from tipster.models import MostYellowCardsPlayer
from tipster.models import MostYellowCardsPlayerPrediction
from tipster.models import MostYellowCardsTeam
from tipster.models import MostYellowCardsTeamPrediction
from tipster.models import Player
from tipster.models import Team
from tipster.models import WorldCupWinner
from tipster.models import WorldCupWinnerPrediction


class TeamAdmin(ModelAdmin):
    list_display = ("name",)


class GameAdmin(ModelAdmin):
    list_display = ("game_name", "team_1", "team_2", "date", "team_1_goals", "team_2_goals")

    def game_name(self, obj):
        return obj


class GamePredictionAdmin(ModelAdmin):
    list_display = ("game_name", "user", "team_1_goals", "team_2_goals", "winner", "created_at", "updated_at")

    search_fields = ["user__username"]

    def game_name(self, obj):
        return obj.game


class PlayerAdmin(ModelAdmin):
    list_display = ("name",)


class MostScorerPredictionAdmin(ModelAdmin):
    list_display = ("player", "user")


class MostAssistsPredictionAdmin(ModelAdmin):
    list_display = ("player", "user")


class MostRedCardsPlayerPredictionAdmin(ModelAdmin):
    list_display = ("player", "user")


class MostYellowCardsPlayerPredictionAdmin(ModelAdmin):
    list_display = ("player", "user")


class WorldCupWinnerPredictionAdmin(ModelAdmin):
    list_display = ("team", "user")


class MostRedCardsTeamPredictionAdmin(ModelAdmin):
    list_display = ("team", "user")


class MostYellowCardsTeamPredictionAdmin(ModelAdmin):
    list_display = ("team", "user")


class WorldCupWinnerAdmin(ModelAdmin):
    list_display = ("team",)


class MostScorerAdmin(ModelAdmin):
    list_display = ("player",)


class MostAssistsAdmin(ModelAdmin):
    list_display = ("player",)


class MostRedCardsPlayerAdmin(ModelAdmin):
    list_display = ("player",)


class MostYellowCardsPlayerAdmin(ModelAdmin):
    list_display = ("player",)


class MostRedCardsTeamAdmin(ModelAdmin):
    list_display = ("team",)


class MostYellowCardsTeamAdmin(ModelAdmin):
    list_display = ("team",)


admin.site.register(Team, TeamAdmin)
admin.site.register(Game, GameAdmin)
admin.site.register(GamePrediction, GamePredictionAdmin)
admin.site.register(Player, PlayerAdmin)
admin.site.register(MostScorerPrediction, MostScorerPredictionAdmin)
admin.site.register(MostAssistsPrediction, MostAssistsPredictionAdmin)
admin.site.register(MostRedCardsPlayerPrediction, MostRedCardsPlayerPredictionAdmin)
admin.site.register(MostYellowCardsPlayerPrediction, MostYellowCardsPlayerPredictionAdmin)
admin.site.register(WorldCupWinnerPrediction, WorldCupWinnerPredictionAdmin)
admin.site.register(MostRedCardsTeamPrediction, MostRedCardsTeamPredictionAdmin)
admin.site.register(MostYellowCardsTeamPrediction, MostYellowCardsTeamPredictionAdmin)
admin.site.register(WorldCupWinner, WorldCupWinnerAdmin)
admin.site.register(MostScorer, MostScorerAdmin)
admin.site.register(MostAssists, MostAssistsAdmin)
admin.site.register(MostRedCardsPlayer, MostRedCardsPlayerAdmin)
admin.site.register(MostYellowCardsPlayer, MostYellowCardsPlayerAdmin)
admin.site.register(MostRedCardsTeam, MostRedCardsTeamAdmin)
admin.site.register(MostYellowCardsTeam, MostYellowCardsTeamAdmin)
