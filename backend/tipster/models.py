from typing import Any

from constance import config
from django.core.exceptions import ValidationError
from django.db.models import DO_NOTHING
from django.db.models import CharField
from django.db.models import DateTimeField
from django.db.models import ForeignKey
from django.db.models import PositiveSmallIntegerField
from django.db.models import UniqueConstraint
from django.utils.timezone import now

from commons import Model

RESULT_MAP = {
    "team_1": "team_1_winner",
    "draw": "draw",
    "team_2": "team_2_winner",
}

RESULT_TYPES = (
    (RESULT_MAP["team_1"], "Team 1 Winner"),
    (RESULT_MAP["draw"], "Draw"),
    (RESULT_MAP["team_2"], "Team 2 Winner"),
)


class Team(Model):
    name = CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Game(Model):
    class Meta:
        constraints = [UniqueConstraint(fields=("team_1", "team_2", "date"), name="game_uniqueness")]

    team_1 = ForeignKey(Team, related_name="team_1", on_delete=DO_NOTHING)
    team_2 = ForeignKey(Team, related_name="team_2", on_delete=DO_NOTHING)
    date = DateTimeField()
    team_1_goals = PositiveSmallIntegerField(blank=True, null=True)
    team_2_goals = PositiveSmallIntegerField(blank=True, null=True)

    @property
    def winner(self):
        if self.team_1_goals > self.team_2_goals:
            return RESULT_MAP["team_1"]
        if self.team_2_goals > self.team_1_goals:
            return RESULT_MAP["team_2"]
        if self.team_1_goals == self.team_2_goals:
            return RESULT_MAP["draw"]

    def __str__(self):
        return f"{self.team_1} vs. {self.team_2}"


class GamePrediction(Model):
    class Meta:
        constraints = [UniqueConstraint(fields=("game", "user"), name="game_prediction_uniqueness")]

    game = ForeignKey(Game, on_delete=DO_NOTHING)
    user = ForeignKey("users.CustomUser", on_delete=DO_NOTHING)
    team_1_goals = PositiveSmallIntegerField()
    team_2_goals = PositiveSmallIntegerField()
    winner = CharField(choices=RESULT_TYPES, max_length=50)
    created_at = DateTimeField(auto_now_add=True)
    updated_at = DateTimeField(auto_now=True)

    def clean(self):
        if now() > self.game.date:
            raise ValidationError("Game start date already passed.")


class Player(Model):
    name = CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class AbstractPlayerPrediction(Model):
    class Meta:
        abstract = True
        constraints = [UniqueConstraint(fields=("player", "user"), name="player_uniqueness")]

    player = ForeignKey(Player, on_delete=DO_NOTHING)
    user = ForeignKey("users.CustomUser", on_delete=DO_NOTHING)
    created_at = DateTimeField(auto_now_add=True)
    updated_at = DateTimeField(auto_now=True)

    def clean(self):
        if games := Game.objects.order_by("date"):
            first_game = games.first()
            if now() > first_game.date and config.ALLOW_OUTDATED_INDIVIDUAL_PREDICTIONS is False:
                raise ValidationError("First game already started.")


class AbstractTeamPrediction(Model):
    class Meta:
        abstract = True
        constraints = [UniqueConstraint(fields=("team", "user"), name="team_uniqueness")]

    team = ForeignKey(Team, on_delete=DO_NOTHING)
    user = ForeignKey("users.CustomUser", on_delete=DO_NOTHING)
    created_at = DateTimeField(auto_now_add=True)
    updated_at = DateTimeField(auto_now=True)

    def clean(self):
        if games := Game.objects.order_by("date"):
            first_game = games.first()
            if now() > first_game.date and config.ALLOW_OUTDATED_INDIVIDUAL_PREDICTIONS is False:
                raise ValidationError("First game already started.")


class MostScorerPrediction(AbstractPlayerPrediction):
    class Meta:
        constraints = [UniqueConstraint(fields=("player", "user"), name="most_scorer_uniqueness")]


class MostAssistsPrediction(AbstractPlayerPrediction):
    class Meta:
        constraints = [UniqueConstraint(fields=("player", "user"), name="most_assists_uniqueness")]


class MostRedCardsPlayerPrediction(AbstractPlayerPrediction):
    class Meta:
        constraints = [UniqueConstraint(fields=("player", "user"), name="most_red_cards_player_uniqueness")]


class WorldCupWinnerPrediction(AbstractTeamPrediction):
    class Meta:
        constraints = [UniqueConstraint(fields=("team", "user"), name="world_cup_winner_uniqueness")]


class MostRedCardsTeamPrediction(AbstractTeamPrediction):
    class Meta:
        constraints = [UniqueConstraint(fields=("team", "user"), name="most_red_card_team_uniqueness")]


class MostYellowCardsPlayerPrediction(AbstractPlayerPrediction):
    class Meta:
        constraints = [UniqueConstraint(fields=("player", "user"), name="most_yellow_cards_player_uniqueness")]


class MostYellowCardsTeamPrediction(AbstractTeamPrediction):
    class Meta:
        constraints = [UniqueConstraint(fields=("team", "user"), name="most_yellow_card_team_uniqueness")]


class WorldCupWinner(Model):
    team = ForeignKey(Team, on_delete=DO_NOTHING)


class MostScorer(Model):
    player = ForeignKey(Player, on_delete=DO_NOTHING)


class MostAssists(Model):
    player = ForeignKey(Player, on_delete=DO_NOTHING)


class MostRedCardsPlayer(Model):
    player = ForeignKey(Player, on_delete=DO_NOTHING)


class MostYellowCardsPlayer(Model):
    player = ForeignKey(Player, on_delete=DO_NOTHING)


class MostRedCardsTeam(Model):
    team = ForeignKey(Team, on_delete=DO_NOTHING)


class MostYellowCardsTeam(Model):
    team = ForeignKey(Team, on_delete=DO_NOTHING)
