from constance import config
from django.db.models import QuerySet
from django.forms import model_to_dict
from django.http import HttpRequest
from rest_framework.generics import get_object_or_404
from rest_framework.mixins import CreateModelMixin
from rest_framework.mixins import ListModelMixin
from rest_framework.mixins import RetrieveModelMixin
from rest_framework.mixins import UpdateModelMixin
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from tipster.models import Game
from tipster.models import GamePrediction
from tipster.models import MostAssistsPrediction
from tipster.models import MostRedCardsPlayerPrediction
from tipster.models import MostRedCardsTeamPrediction
from tipster.models import MostScorerPrediction
from tipster.models import MostYellowCardsPlayerPrediction
from tipster.models import MostYellowCardsTeamPrediction
from tipster.models import Player
from tipster.models import Team
from tipster.models import WorldCupWinnerPrediction
from tipster.serializers import GamePredictionSerializer
from tipster.serializers import MostAssistsPredictionSerializer
from tipster.serializers import MostRedCardsPlayerPredictionSerializer
from tipster.serializers import MostRedCardsTeamPredictionSerializer
from tipster.serializers import MostScorerPredictionSerializer
from tipster.serializers import MostYellowCardsPlayerPredictionSerializer
from tipster.serializers import MostYellowCardsTeamPredictionSerializer
from tipster.serializers import PlayerSerializer
from tipster.serializers import WorldCupWinnerPredictionSerializer


class TeamsView(APIView):
    def get(self, request: HttpRequest) -> Response:
        return Response([{"id": team.id, "name": team.name} for team in Team.objects.all().order_by("name")])


class GamePredictionViewSet(CreateModelMixin, UpdateModelMixin, RetrieveModelMixin, GenericViewSet):
    serializer_class = GamePredictionSerializer

    def get_queryset(self) -> QuerySet:
        return GamePrediction.objects.filter(user=self.request.user)


class GamePredictionByGameIdView(APIView):
    def get(self, request: HttpRequest, *args, **kwargs) -> Response:
        game_prediction = get_object_or_404(GamePrediction.objects.filter(user=request.user), game__id=kwargs["id"])
        return Response(model_to_dict(game_prediction, exclude=("game", "user")))


class GamesView(APIView):
    def get(self, request: HttpRequest) -> Response:
        return Response(
            [
                {
                    "id": game.id,
                    "team_1": game.team_1.name,
                    "team_2": game.team_2.name,
                    "date": game.date,
                    "team_1_goals": game.team_1_goals,
                    "team_2_goals": game.team_2_goals,
                }
                for game in Game.objects.all()
            ]
        )


class PlayerViewSet(CreateModelMixin, ListModelMixin, GenericViewSet):
    serializer_class = PlayerSerializer
    model = Player

    def get_queryset(self):
        return self.model.objects.all()


class ConfiguredViewSet(CreateModelMixin, UpdateModelMixin, RetrieveModelMixin, GenericViewSet):
    model = None

    def get_queryset(self):
        return self.model.objects.filter(user=self.request.user)


class MostScorerPredictionViewSet(ConfiguredViewSet):
    serializer_class = MostScorerPredictionSerializer
    model = MostScorerPrediction


class MostAssistsPredictionViewSet(ConfiguredViewSet):
    serializer_class = MostAssistsPredictionSerializer
    model = MostAssistsPrediction


class MostRedCardsPlayerPredictionViewSet(ConfiguredViewSet):
    serializer_class = MostRedCardsPlayerPredictionSerializer
    model = MostRedCardsPlayerPrediction


class MostYellowCardsPlayerPredictionViewSet(ConfiguredViewSet):
    serializer_class = MostYellowCardsPlayerPredictionSerializer
    model = MostYellowCardsPlayerPrediction


class WorldCupWinnerPredictionViewSet(ConfiguredViewSet):
    serializer_class = WorldCupWinnerPredictionSerializer
    model = WorldCupWinnerPrediction


class MostRedCardsTeamPredictionViewSet(ConfiguredViewSet):
    serializer_class = MostRedCardsTeamPredictionSerializer
    model = MostRedCardsTeamPrediction


class MostYellowCardsTeamPredictionViewSet(ConfiguredViewSet):
    serializer_class = MostYellowCardsTeamPredictionSerializer
    model = MostYellowCardsTeamPrediction


class CustomUserPredictions(APIView):
    def get(self, request: HttpRequest, *args, **kwargs) -> Response:
        return Response(
            {
                "most_scorer_prediction": model_to_dict(
                    MostScorerPrediction.objects.get(user=request.user), exclude=("user")
                )
                if MostScorerPrediction.objects.filter(user=request.user).exists()
                else None,
                "most_assists_prediction": model_to_dict(
                    MostAssistsPrediction.objects.get(user=request.user), exclude=("user")
                )
                if MostAssistsPrediction.objects.filter(user=request.user).exists()
                else None,
                "most_red_cards_player_prediction": model_to_dict(
                    MostRedCardsPlayerPrediction.objects.get(user=request.user), exclude=("user")
                )
                if MostRedCardsPlayerPrediction.objects.filter(user=request.user).exists()
                else None,
                "most_yellow_cards_player_prediction": model_to_dict(
                    MostYellowCardsPlayerPrediction.objects.get(user=request.user), exclude=("user")
                )
                if MostYellowCardsPlayerPrediction.objects.filter(user=request.user).exists()
                else None,
                "world_cup_winner_prediction": model_to_dict(
                    WorldCupWinnerPrediction.objects.get(user=request.user), exclude=("user")
                )
                if WorldCupWinnerPrediction.objects.filter(user=request.user).exists()
                else None,
                "most_red_cards_team_prediction": model_to_dict(
                    MostRedCardsTeamPrediction.objects.get(user=request.user), exclude=("user")
                )
                if MostRedCardsTeamPrediction.objects.filter(user=request.user).exists()
                else None,
                "most_yellow_cards_team_prediction": model_to_dict(
                    MostYellowCardsTeamPrediction.objects.get(user=request.user), exclude=("user")
                )
                if MostYellowCardsTeamPrediction.objects.filter(user=request.user).exists()
                else None,
            }
        )


class FirstGameDateTimeView(APIView):
    def get(self, request: HttpRequest) -> Response:
        return Response({"date_time": Game.objects.order_by("date").first().date})


class ConfigView(APIView):
    def get(self, request: HttpRequest) -> Response:
        return Response({"ALLOW_OUTDATED_INDIVIDUAL_PREDICTIONS": config.ALLOW_OUTDATED_INDIVIDUAL_PREDICTIONS})
