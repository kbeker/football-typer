#!/bin/bash
set -euo pipefail

cd ${BASH_SOURCE%/*}/../

pipenv run pytest                         \
	--rootdir=.                             \
	--cov-report term-missing 	            \
	--cov-config "scripts/coverage-config"  \
	--cov=.
rm .coverage
