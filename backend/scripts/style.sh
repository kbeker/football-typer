#!/bin/bash
set -euo pipefail

printf "[ISORT: FootballTyper Webapp]\n"
${BASH_SOURCE%/*}/find-files-to-check.sh | xargs pipenv run isort --sl -l 120
printf "\n"

printf "[BLACK: FootballTyper Webapp]\n"
${BASH_SOURCE%/*}/find-files-to-check.sh | xargs pipenv run black --line-length 120 --target-version py310
printf "\n"
