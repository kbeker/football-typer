#!/bin/bash
set -euo pipefail

cd ${BASH_SOURCE%/*}/
MYPY_CONFIG_FILE_PATH=`pwd`/mypy.ini
cd ..

printf "[MYPY: FootballTyper]\n"

pipenv run mypy --config-file="${MYPY_CONFIG_FILE_PATH}" .
cd ..
