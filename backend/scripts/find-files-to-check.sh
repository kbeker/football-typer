#!/bin/bash -e

find ${BASH_SOURCE%/*}/.. \
    -type f               \
    -name "*.py"
