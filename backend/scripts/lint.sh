#!/bin/bash
set -euo pipefail

printf "[FLAKE8: FootballTyper Webapp]\n"
${BASH_SOURCE%/*}/find-files-to-check.sh | xargs pipenv run flake8                                          \
    --jobs=4                                                                                                \
    --max-line-length=120                                                                                   \
    --ignore=E124,E126,E128,E131,E156,E201,E221,E222,E231,E241,E265,E271,E272,E701,F405,E501,W503           \

printf "\n"

printf "[PYLINT: FootballTyper Webapp]\n"
${BASH_SOURCE%/*}/find-files-to-check.sh | xargs pipenv run pylint --rcfile=${BASH_SOURCE%/*}/pylintrc
printf "\n"
