from rest_framework.generics import CreateAPIView
from rest_framework.permissions import AllowAny

from users.auth.serializers import SignUpSerializer
from users.models import CustomUser


class SignUpView(CreateAPIView):
    authentication_classes = []
    queryset = CustomUser.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = SignUpSerializer
