from typing import Dict

from django.contrib.auth.password_validation import validate_password
from rest_framework.exceptions import ValidationError
from rest_framework.fields import CharField
from rest_framework.fields import EmailField
from rest_framework.serializers import ModelSerializer
from rest_framework.validators import UniqueValidator

from users.models import CustomUser


class SignUpSerializer(ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ["email", "first_name", "last_name", "password", "password2"]

    email = EmailField(required=True, validators=[UniqueValidator(queryset=CustomUser.objects.all())])
    first_name = CharField(required=True)
    last_name = CharField(required=True)
    password = CharField(write_only=True, required=True, validators=[validate_password])
    password2 = CharField(write_only=True, required=True)

    def validate(self, attrs: Dict) -> Dict:
        if attrs["password"] != attrs["password2"]:
            raise ValidationError({"password": "Password fields didn't match."})

        return attrs

    def create(self, validated_data: Dict) -> CustomUser:
        user = CustomUser(
            username=validated_data["email"],
            email=validated_data["email"],
            first_name=validated_data["first_name"],
            last_name=validated_data["last_name"],
        )
        user.set_password(validated_data["password"])
        user.save()

        return user
