from rest_framework.mixins import RetrieveModelMixin
from rest_framework.viewsets import GenericViewSet

from users.models import CustomUser
from users.serializers import CustomUserSerializer


class CustomUserViewSet(RetrieveModelMixin, GenericViewSet):
    serializer_class = CustomUserSerializer

    def get_queryset(self):
        return CustomUser.objects.filter(id=self.request.user.id)
