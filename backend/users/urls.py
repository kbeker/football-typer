from django.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter

from users.views import CustomUserViewSet

router = DefaultRouter()
router.register("user", CustomUserViewSet, basename="user")

urlpatterns = [path("", include(router.urls))]
