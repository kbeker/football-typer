from django.contrib.auth.models import AbstractUser

from commons import Model
from tipster.models import WorldCupWinner, MostScorer, MostAssists, MostRedCardsPlayer, MostYellowCardsPlayer, \
    MostRedCardsTeam, MostYellowCardsTeam


class CustomUser(AbstractUser, Model):
    @property
    def points(self):
        points_from_games = 0
        for game_prediction in self.gameprediction_set.all():
            if game_prediction.game.team_1_goals is not None and game_prediction.game.team_2_goals is not None:
                if (
                    game_prediction.game.team_1_goals == game_prediction.team_1_goals
                    and game_prediction.game.team_2_goals == game_prediction.team_2_goals
                ):
                    points_from_games += 3
                if game_prediction.game.winner == game_prediction.winner:
                    points_from_games += 1

        if self.worldcupwinnerprediction_set.exists():
            if WorldCupWinner.objects.filter(team=self.worldcupwinnerprediction_set.first().team).exists():
                points_from_games += 10

        if self.mostscorerprediction_set.exists():
            if MostScorer.objects.filter(player=self.mostscorerprediction_set.first().player).exists():
                points_from_games += 5

        if self.mostassistsprediction_set.exists():
            if MostAssists.objects.filter(player=self.mostassistsprediction_set.first().player).exists():
                points_from_games += 5

        if self.mostredcardsplayerprediction_set.exists():
            if MostRedCardsPlayer.objects.filter(player=self.mostredcardsplayerprediction_set.first().player).exists():
                points_from_games += 5

        if self.mostyellowcardsplayerprediction_set.exists():
            if MostYellowCardsPlayer.objects.filter(player=self.mostyellowcardsplayerprediction_set.first().player).exists():
                points_from_games += 5

        if self.mostredcardsteamprediction_set.exists():
            if MostRedCardsTeam.objects.filter(team=self.mostredcardsteamprediction_set.first().team).exists():
                points_from_games += 5

        if self.mostyellowcardsteamprediction_set.exists():
            if MostYellowCardsTeam.objects.filter(team=self.mostyellowcardsteamprediction_set.first().team).exists():
                points_from_games += 5

        return points_from_games
