#!/bin/bash
set -euo pipefail
export DJANGO_SETTINGS_MODULE=backend.settings

function argument_parser {
  for i in "$@"; do
   case $i in
     --skip-packages-reinstall)
       REINSTALL_PACKAGES="false"
       shift
       ;;
     -*|--*)
       echo "Unknown option $i"
       exit 1
       ;;
     *)
       ;;
   esac
  done
}

REINSTALL_PACKAGES="true"

argument_parser "$@"

printf "Providing checks for ${BASH_SOURCE%/*}/webapp directory\n"

if [ "${REINSTALL_PACKAGES}" == "true" ]
then
  printf "=============== VIRTUAL ENVIRONMENT PACKAGES CHECKS ================\n"
  ${BASH_SOURCE%/*}/scripts/check-environment-packages.sh
  printf "\n"
fi

printf "=================== DJANGO CONFIGURATION CHECKS ====================\n"
pipenv run python ${BASH_SOURCE%/*}/manage.py check
printf "\n"

printf "======================= MIGRATION FILES CHECK ======================\n"
pipenv run ${BASH_SOURCE%/*}/manage.py makemigrations --check --dry-run
printf "\n"

printf "=========================== CODING STYLE ===========================\n"
${BASH_SOURCE%/*}/scripts/style.sh
printf "\n"

printf "=============================== LINT ===============================\n"
${BASH_SOURCE%/*}/scripts/lint.sh
printf "\n"

printf "========================= MYPY STATIC TYPE CHECKER =================\n"
${BASH_SOURCE%/*}/scripts/typing.sh
printf "\n"

printf "========================= UNIT TESTS WITH COVERAGE =================\n"
${BASH_SOURCE%/*}/scripts/run-test-coverage.sh
printf "\n"
