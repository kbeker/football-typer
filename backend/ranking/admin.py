from django.contrib import admin
from django.contrib.admin import ModelAdmin

from ranking.models import ChampionsRanking


class ChampionsRankingAdmin(ModelAdmin):
    list_display = ("id", "user")


admin.site.register(ChampionsRanking, ChampionsRankingAdmin)
