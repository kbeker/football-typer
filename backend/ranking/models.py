from django.db.models import CASCADE
from django.db.models import ForeignKey

from commons import Model
from users.models import CustomUser


class ChampionsRanking(Model):
    user = ForeignKey(CustomUser, on_delete=CASCADE)
