from rest_framework.mixins import ListModelMixin
from rest_framework.viewsets import GenericViewSet

from ranking.models import ChampionsRanking
from ranking.serializers import ChampionsRankingSerializer


class ChampionsRankingViewSet(ListModelMixin, GenericViewSet):
    serializer_class = ChampionsRankingSerializer

    def get_queryset(self):
        return ChampionsRanking.objects.all()
