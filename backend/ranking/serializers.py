from rest_framework.fields import ReadOnlyField
from rest_framework.serializers import ModelSerializer

from ranking.models import ChampionsRanking


class ChampionsRankingSerializer(ModelSerializer):
    class Meta:
        model = ChampionsRanking
        fields = ["id", "user", "first_name", "last_name", "points"]

    points = ReadOnlyField(source="user.points")
    first_name = ReadOnlyField(source="user.first_name")
    last_name = ReadOnlyField(source="user.last_name")
