from django.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter

from ranking.views import ChampionsRankingViewSet

router = DefaultRouter()
router.register("ranking", ChampionsRankingViewSet, basename="user_ranking")

urlpatterns = [path("", include(router.urls))]
