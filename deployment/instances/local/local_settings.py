from .prod import *  # noqa: F401, F403

DATABASES = {
    "default": {
        "NAME": "football_typer",
        "USER": "postgres",
        "PASSWORD": "postgres",
        "HOST": "postgres",
        "PORT": "5432",
        "ENGINE": "django.db.backends.postgresql_psycopg2"
    }
}

STATIC_ROOT = "/usr/src/app/static-files-data/"

ALLOWED_HOSTS = ["*"]

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"

SECRET_KEY = "local"

SITE_PROTOCOL = "http"
SITE_URL = "127.0.0.1"
STATIC_URL = "/statics/"
