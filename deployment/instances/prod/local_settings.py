from .prod import *  # noqa: F401, F403
from .secrets import *  # noqa: F401, F403

DATABASES = {
    "default": {
        "NAME": "football_typer",
        "USER": "postgres",
        "PASSWORD": "postgres",
        "HOST": "postgres",
        "PORT": "5432",
        "ENGINE": "django.db.backends.postgresql_psycopg2"
    }
}

STATIC_ROOT = "/usr/src/app/static-files-data/"

ALLOWED_HOSTS = ["*"]

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"

STATIC_URL = "/statics/"

CSRF_TRUSTED_ORIGINS = ["https://kanapowi-pilkarze.pl"]
