#!/bin/bash -e

backup_archive_name=backup_$(date '+%d_%m_%Y').tar.gz
tar -zcvf /home/gitlab/backups/$backup_archive_name /app-data/pgdata
aws s3 cp /home/gitlab/backups/$backup_archive_name s3://kanapowi-pilarze-CI_ENVIRONMENT_NAME/
rm /home/gitlab/backups/$backup_archive_name
